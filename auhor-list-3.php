<?php
/**
 *	Template to display the authors.
 *	Template Name: Author List
 *
 *	@package protopress
 **/

get_header(); ?>

    <div id="primary" class="content-areas <?php do_action('protopress_primary-width') ?>">
        <main id="main" class="site-main" role="main">
            <?php setup_userdata(); ?>
            <?php $args	=	array(
                'role'	=>	'contributor',
            ); ?>
            <?php $users = get_users( $args );
            //var_dump($users);?>
            <div class="wrapper">
                <div class="col-md-12 col-sm-12">

                    <?php foreach ($users as $user) {
                        $FirstName	=	get_user_meta($user->ID, 'first_name', true);
                        $LastName	=	get_user_meta($user->ID, 'last_name', true); ?>
                        <div class="col-md-4 col-sm-4 author-outer">
                            <div class="author-inner">
                                <a href="<?php echo get_author_posts_url($user->ID) ?>">
                                    <div class="author_profile_image" style="background-image: url(<?php echo get_avatar_url($user->ID,150); ?>); background-size: cover;"></div>
                                    <?php echo get_avatar( $user->ID,150 ); ?>

                                    <div class="author-del">
                                        <div class="author_name"><span><?php echo $FirstName . ' ' . $LastName ?></span></div>
                                        <?php
                                        $post_count = count_user_posts( $user->ID );
                                        $rating_res = star_rating($post_count);?>
                                        <div class="stars">

                                            <div class="5-star">
                                                <?php for($i = 1; $i <= 5; $i++){?>
                                                    <?php echo "<i class='far fa-star'></i>";?><?php }?>
                                            </div>
                                            <div class="rating-stars">
                                                <?php for($i = 1; $i <= $rating_res; $i++){?>
                                                    <?php echo "<i class='fas fa-star'></i>";?><?php }?>
                                            </div>

                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php }?>
                </div>

            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>