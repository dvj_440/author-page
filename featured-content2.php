<?php function protopress_boxes() { ?>
	<div class="popular-articles col-md-6">
		<div class="section-title">
			<?php echo get_theme_mod('protopress_box_title','Popular Articles'); ?>
		</div>	
		
		<?php /* Start the Loop */ $count=0;?>
				<?php
		    		$args = array( 'posts_per_page' => 4, 'cat' => get_theme_mod('protopress_box_cat') );
					$lastposts = get_posts( $args );
					$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) :

							
							$loop->the_post(); ?>
				
				    <div class="col-md-6 col-sm-6 imgcontainer">
				    	<div class="popimage">
				        <?php if (has_post_thumbnail()) : ?>	
								<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"><?php the_post_thumbnail('pop-thumb'); ?></a>
						<?php else : ?>
								<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"><img src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
						<?php endif; ?>
							<div class="titledesc">
				            <h2><?php echo the_title(); ?></h2>
				            <a class="hvr-float-shadow readmorelink" href="<?php the_permalink() ?>"><?php _e('Read More','protopress'); ?></a>
				        </div>
				    	</div>	
				        
				    </div>
				    
				<?php 
					$count++;
				    if ($count == 4) 
				    	break;

				endwhile;
				 
				 wp_reset_postdata(); ?>
		
	</div>
	<?php }
	
	function protopress_slider() { ?>
		<div class="latest-hap col-md-6">
		<div class="section-title">
				<?php echo get_theme_mod('protopress_slider_title','Latest Happenings'); ?>
			</div>		
			<ul id="sb-slider" class="bxslider">
				
			 
			 	<?php /* Start the Loop */ ?>
						<?php
				    		$args = array( 'posts_per_page' => get_theme_mod('protopress_slider_count', 4 ), 'cat' => get_theme_mod('protopress_slider_cat') );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) :

							
							$loop->the_post(); 
							
														  
							if ( has_post_thumbnail() ) :
				        		$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID), 'slider-thumb' ); 
								$image_url = $image_data[0]; 
							endif;
							
							if ($image_url != '') : //Don't Display Anything if no image. ?>	  
						    <li>
						        
								<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"><?php the_post_thumbnail('slider-thumb'); ?></a>
						        <div class="sb-description">
						            <h3><?php echo the_title(); ?></h3>
						        </div>
						    </li>
						    <?php endif; ?>
						    
						<?php endwhile;
							wp_reset_query(); ?>    
			</ul>
			<div id="nav-arrows" class="nav-arrows">
				<a id="slider-next"></a>
				<a id="slider-prev"></a>
			</div>
		</div>
	<?php }	
	
?>

<div class="featured-2">
<div class="container">
	
	<?php if ( get_theme_mod('protopress_box_position', 'left') == 'right') :
		protopress_slider();
		protopress_boxes();
	else :
		protopress_boxes();	
		protopress_slider();
	endif; ?>	
		
	
	
</div><!--.container-->
</div>
