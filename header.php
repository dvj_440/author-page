<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package protopress
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php 
if ( is_home() ) :
	//require_once(get_template_directory().'/switcher/switcher.php'); 
endif;
?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'protopress' ); ?></a>
	<div id="jumbosearch">
		<span class="fa fa-remove closeicon"></span>
		<div class="form">
			<?php get_search_form(); ?>
		</div>
	</div>	
		
	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<div class="site-branding col-md-3">
				<?php if ( get_theme_mod('protopress_logo') != "" ) : ?>
				<div id="site-logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_theme_mod('protopress_logo'); ?>"></a>
				</div>
				<?php endif; ?>
				<div id="text-title-desc">
				<h1 class="site-title title-font"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</div>
			</div>	
			
			<?php if ( get_theme_mod('protopress_topad') ) : ?>
			<div id="top-ad">
				<div class="adcontainer">
					<?php echo get_theme_mod('protopress_topad'); ?>
				</div>
			</div>	
			<?php endif; ?>
			
		<nav id="site-navigation" class="main-navigation col-md-8" role="navigation">
			<div class="container">
				<?php $walker = new Protopress_Menu_With_Description; ?>
				<?php if (has_nav_menu(  'primary' ) && !get_theme_mod('protopress_disable_nav_desc', true) ) :
							wp_nav_menu( array( 'theme_location' => 'primary', 'walker' => $walker ) ); 
						else : 
							wp_nav_menu( array( 'theme_location' => 'primary' ) );
					  endif; ?>
			</div>
		</nav><!-- #site-navigation -->
		<a id="searchicon" class="col-md-1">
				<span class="fa fa-search"></span>
			</a>
			
		</div>	
		
		<div id="slickmenu"></div>
		
	</header><!-- #masthead -->
	
	<div class="mega-container">
	
		<?php do_action('protopress_after-mega-container'); ?>
		
	
		<div id="content" class="site-content container">