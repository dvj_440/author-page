jQuery(document).ready( function() {
	jQuery('#searchicon').click(function() {
		jQuery('#jumbosearch').fadeIn();
		jQuery('#jumbosearch input').focus();
	});
	jQuery('#jumbosearch .closeicon').click(function() {
		jQuery('#jumbosearch').fadeOut();
	});
	jQuery('body').keydown(function(e){
	    
	    if(e.keyCode == 27){
	        jQuery('#jumbosearch').fadeOut();
	    }
	});
	
	jQuery('.flex-images').flexImages({rowHeight: 200, object: 'img', truncate: true});
	
	//jQuery('#sb-slider').slicebox();
	
	jQuery('#site-navigation ul.menu').slicknav({
		label: 'Menu',
		duration: 1000,
		prependTo:'#slickmenu'
	});	
	
	respond();
	
});

jQuery(window).load(function() {
        jQuery('#nivoSlider').nivoSlider({
	        prevText: "<i class='fa fa-chevron-circle-left'></i>",
	        nextText: "<i class='fa fa-chevron-circle-right'></i>",
	        controlNav: slider_object.pager,
	        animSpeed: parseInt(slider_object.animSpeed),                
			pauseTime: parseInt(slider_object.pauseTime),
			manualAdvance: slider_object.autoplay,
			randomStart: slider_object.random,
			effect: slider_object.effect,
	        
        });
    });
    

function respond() {
	if( jQuery(window).width() < 992 ) {
		jQuery("article.photos_3_column").unwrap();
	}
}


jQuery(document).ready(function(){
  jQuery('.bxslider').bxSlider({
	  nextSelector: '#slider-next',
	  prevSelector: '#slider-prev',
	  nextText: "<i class='fa fa-chevron-right'></i>",
	  prevText: "<i class='fa fa-chevron-left'></i>",
	  pager: false,
	  auto: true,
	  mode: 'fade',
  });
});

