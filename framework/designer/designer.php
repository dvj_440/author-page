<?php
/*
*
* Dynamically Design the theme using Less Compiler for PHP
* Compiler Runs only when Customizer is Loaded, not for users. So no effect on site performance.
*
*/	
require get_template_directory() ."/framework/designer/lessc.inc.php";


function protopress_exec_less() {
	$less = new lessc;
	$inputFile = get_template_directory() ."/assets/less/custom.less";
	$outputFile = get_template_directory() ."/assets/css/custom.css";

	$less->setVariables(array(
		"accent" => get_theme_mod('protopress_skinx_var_accent','#42a1cd'),	
		"background" => get_theme_mod('protopress_skinx_var_background','#fff'),
		"header-bg" => get_theme_mod('protopress_skinx_var_headerbg','#ffffff'),
		"onaccent" => get_theme_mod('protopress_skinx_var_onaccent','#FFFFFF'),
		"sbg" => get_theme_mod('protopress_skinx_var_sbg','#F4f4f4'),
		"content" => get_theme_mod('protopress_skinx_var_content','#777777'),
	  
	));
	
	
	if ( is_customize_preview() )  {
		try {
			$less->compileFile( $inputFile, $outputFile ); 
		} catch(exception $e) {
			echo "fatal error: " . $e->getMessage();
		}
		
	} 
	else {
		$less->checkedCompile( $inputFile, $outputFile );
	}

}	
add_action('wp_head','protopress_exec_less', 1);