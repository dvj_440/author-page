<?php
/*
 * @package protopress, Copyright Rohit Tripathi, rohitink.com
 * This file contains Custom Theme Related Functions.
 */
 
class Protopress_Menu_With_Description extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$fontIcon = ! empty( $item->attr_title ) ? ' <i class="fa ' . esc_attr( $item->attr_title ) .'">' : '';
		$attributes = ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>'.$fontIcon.'</i>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<br /><span class="menu-desc">' . $item->description . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id );
	}
}

/*
 * Pagination Function. Implements core paginate_links function.
 */
function protopress_pagination() {
	global $wp_query;
	$big = 12345678;
	$page_format = paginate_links( array(
	    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format' => '?paged=%#%',
	    'current' => max( 1, get_query_var('paged') ),
	    'total' => $wp_query->max_num_pages,
	    'type'  => 'array'
	) );
	if( is_array($page_format) ) {
	            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
	            echo '<div class="pagination"><div><ul>';
	            echo '<li><span>'. $paged . ' of ' . $wp_query->max_num_pages .'</span></li>';
	            foreach ( $page_format as $page ) {
	                    echo "<li>$page</li>";
	            }
	           echo '</ul></div></div>';
	 }
}
//Quick Fixes for Custom Post Types.
function protopress_pagination_queried( $query ) {
	$big = 12345678;
	$page_format = paginate_links( array(
	    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format' => '?paged=%#%',
	    'current' => max( 1, get_query_var('paged') ),
	    'total' => $query->max_num_pages,
	    'type'  => 'array'
	) );
	if( is_array($page_format) ) {
	            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
	            echo '<div class="pagination"><div><ul>';
	            echo '<li><span>'. $paged . ' of ' . $query->max_num_pages .'</span></li>';
	            foreach ( $page_format as $page ) {
	                    echo "<li>$page</li>";
	            }
	           echo '</ul></div></div>';
	 }
}

/*
** Customizer Controls 
*/
if (class_exists('WP_Customize_Control')) {
	class WP_Customize_Category_Control extends WP_Customize_Control {
        /**
         * Render the control's content.
         */
        public function render_content() {
            $dropdown = wp_dropdown_categories(
                array(
                    'name'              => '_customize-dropdown-categories-' . $this->id,
                    'echo'              => 0,
                    'show_option_none'  => __( '&mdash; Select &mdash;', 'protopress' ),
                    'option_none_value' => '0',
                    'selected'          => $this->value(),
                )
            );
 
            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
 
            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );
        }
    }
}    
/*
** Function to check if Sidebar is enabled on Current Page 
*/
function protopress_load_sidebar() {
	wp_reset_postdata();
	$load_sidebar = true;
	if ( get_theme_mod('protopress_disable_sidebar') ) :
		$load_sidebar = false;
	elseif( get_theme_mod('protopress_disable_sidebar_home') && is_home() )	:
		$load_sidebar = false;
	elseif( get_theme_mod('protopress_disable_sidebar_front') && is_front_page() ) :
		$load_sidebar = false;
	elseif( get_theme_mod('protopress_disable_sidebar_archive') && is_archive() ) :
		$load_sidebar = false;	
	elseif ( get_post_meta( get_the_ID(), 'enable-full-width', true ) )	:
		$load_sidebar = false;
	endif;
	
	return  $load_sidebar;
}

/*
**	Determining Sidebar and Primary Width
*/
function protopress_primary_class() {
	$sw = get_theme_mod('protopress_sidebar_width',4);
	$class = "col-md-".(12-$sw);
	
	wp_reset_postdata();
	if ( !protopress_load_sidebar() ) 
		$class = "col-md-12";
	
	echo $class;
}
add_action('protopress_primary-width', 'protopress_primary_class');

function protopress_secondary_class() {
	$sw = get_theme_mod('protopress_sidebar_width',4);
	$class = "col-md-".$sw;
	
	echo $class;
}
add_action('protopress_secondary-width', 'protopress_secondary_class');


/*
**	Helper Function to Convert Colors
*/
function protopress_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);
   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return implode(",", $rgb); // returns the rgb values separated by commas
   //return $rgb; // returns an array with the rgb values
}
function protopress_fade($color, $val) {
	return "rgba(".protopress_hex2rgb($color).",". $val.")";
}


/*
** Function to Get Theme Layout 
*/
function protopress_get_blog_layout(){
	static $protopress_post_count = 0;
	$ldir = 'framework/layouts/content';
	if (get_theme_mod('protopress_blog_layout') ) :
		get_template_part( $ldir , get_theme_mod('protopress_blog_layout') );
	else :
		get_template_part( $ldir ,'grid');	
	endif;	
}
add_action('protopress_blog_layout', 'protopress_get_blog_layout');

/*
** Function to Deal with Elements of Inequal Heights, Enclose them in a bootstrap row.
*/
function protopress_open_div_row() {
	echo "<div class='row grid-row col-md-12'>";
}
function protopress_close_div_row() {
	echo "</div><!--.grid-row-->";
}


function protopress_before_article() {

	global $protopress_post_count;
	$array_2_3_4 = array('grid_2_column',
							'grid_3_column',
							'grid_4_column',
							'photos_3_column',
							'photos_2_column',							
							'templates/template-blog-grid3c.php',
							'templates/template-blog-grid2c.php', 
							'templates/template-blog-grid4c.php',
							'templates/template-blog-photos3c.php',
							'templates/template-blog-photos2c.php'
						);
	//wp_reset_postdata();	- Don't Reset any Data, because we are not using get_post_meta	
	//See what the get_queried_object_id() function does. Though, the Query is reset in template files.			
	//For 2,3,4 Column Posts
	 $page_template = get_post_meta( get_queried_object_id(), '_wp_page_template', true );
	if ( in_array( get_theme_mod('protopress_blog_layout'), $array_2_3_4 ) || in_array( $page_template, $array_2_3_4 ) ) : 
			 if ( $protopress_post_count  == 0 ) {
			  	protopress_open_div_row();
			  }
	endif;	  	
}
add_action('protopress_before-article', 'protopress_before_article');

/* Pre and Post Article Hooking */
function protopress_after_article() {
	global $protopress_post_count;
	//echo $protopress_post_count;
	wp_reset_postdata();
	$template = get_post_meta( get_the_id(), '_wp_page_template', true );
	//For 3 Column Posts
	if (   ( get_theme_mod('protopress_blog_layout') == 'grid_3_column' ) 
		|| ( get_theme_mod('protopress_blog_layout') == 'photos_3_column' )
 		|| ( $template == 'templates/template-blog-grid3c.php' )
 		|| ( $template == 'templates/template-blog-photos3c.php' ) ):
		
		

		global $wp_query;
		if (($wp_query->current_post +1) == ($wp_query->post_count)) :
			 	protopress_close_div_row();
		else :
			if ( ( $protopress_post_count ) == 2 ) {
			 	protopress_close_div_row();
				$protopress_post_count = 0;
				}
			else {
				$protopress_post_count++;
			}
		endif;		
		
	//For 2 Column Posts
	elseif ( ( get_theme_mod('protopress_blog_layout') == 'grid_2_column' )
		|| ( get_theme_mod('protopress_blog_layout') == 'photos_2_column' )
		|| ( $template == 'templates/template-blog-grid2c.php' )
		|| ( $template == 'templates/template-blog-photos2c.php' ) ):
		
		
		
		global $wp_query;
		if (($wp_query->current_post +1) == ($wp_query->post_count)) :
			 	protopress_close_div_row();
			 	$protopress_post_count = 0;
		else :
			if ( ( $protopress_post_count ) == 1 ) {
			 	protopress_close_div_row();
				$protopress_post_count = 0;
				}
			else {
				$protopress_post_count++;
			}
		endif;		
	
	elseif ( ( get_theme_mod('protopress_blog_layout') == 'grid_4_column' )
		|| ( $template == 'templates/template-blog-grid4c.php' ) ):
		
		
		
		global $wp_query;
		if (($wp_query->current_post +1) == ($wp_query->post_count)) :
			 	protopress_close_div_row();
		else :
			if ( ( $protopress_post_count ) == 3 ) {
			 	protopress_close_div_row();
				$protopress_post_count = 0;
				}
			else {
				$protopress_post_count++;
			}
		endif;		
	endif;
	
}
add_action('protopress_after-article', 'protopress_after_article');

/*
** Function to check if Component is Enabled.
*/
function protopress_is_enabled( $component ) {
	
	wp_reset_postdata();
	$return_val = false;
	
	switch ($component) {
		
		case 'slider' :
		
			if ( ( get_theme_mod('protopress_main_slider_enable' ) && is_home() )
				||( get_post_meta( get_the_ID(), 'enable-slider', true ) ) ) :
					$return_val = true;
			endif;
			break;
		
		case 'fc1' :
		
			if ( ( get_theme_mod('protopress_grid_enable') && is_home() )
				|| ( get_post_meta( get_the_ID(), 'enable-img-grid', true ) ) ) :
					$return_val = true;
				endif;
				break;
		
		case 'fc2' :
		
			 if ( ( get_theme_mod('protopress_box_enable') && is_home() )
			 	|| ( get_post_meta( get_the_ID(), 'enable-sqbx', true ) ) ) : 
			 		$return_val = true;
			 	endif;
			 	break;		 		
									
	}//endswitch
	
	return $return_val;
	
}

/*
**	Hook Just before content. To Display Featured Content and Slider.
*/
function protopress_display_fc() {
	
		if  ( protopress_is_enabled( 'slider' ) )
			get_template_part('slider', 'nivo' );
			
		if  ( protopress_is_enabled( 'fc2' ) )	
			get_template_part('featured', 'content2');
		
		if  ( protopress_is_enabled( 'fc1' ) )	
			get_template_part('featured', 'content1'); 
		
}
add_action('protopress_after-mega-container', 'protopress_display_fc');


/*
** ProtoPress Render Slider
*/
function protopress_render_slider() {
	$protopress_slider = array(
		'pager' => get_theme_mod('protopress_slider_pager', true ),
		'animSpeed' => get_theme_mod('protopress_slider_speed', 500 ),
		'pauseTime' => get_theme_mod('protopress_slider_pause', 5000 ),
		'autoplay' => !get_theme_mod('protopress_slider_autoplay', true ),
		'random' => get_theme_mod('protopress_slider_random', false ),
		'effect' => get_theme_mod('protopress_slider_effect', 'random' )
	);
	wp_localize_script( 'protopress-custom-js', 'slider_object', $protopress_slider );
}
add_action('wp_enqueue_scripts', 'protopress_render_slider', 20);

/*
** Migrate Old CSS to WP 4.7
*/
if ( function_exists( 'wp_update_custom_css_post' ) ) {
    // Migrate any existing theme CSS to the core option added in WordPress 4.7.
    $css = get_theme_mod( 'protopress_custom_css' );
    if ( $css ) {
        $core_css = wp_get_custom_css(); // Preserve any CSS already added to the core option.
        $return = wp_update_custom_css_post( $core_css . $css );
        if ( ! is_wp_error( $return ) ) {
            // Remove the old theme_mod, so that the CSS is stored in only one place moving forward.
            remove_theme_mod( 'custom_theme_css' );
        }
    }
} else {
    // Back-compat for WordPress < 4.7.
   } 
    
/*
** Load WooCommerce Compatibility FIle
*/
if ( class_exists('woocommerce') ) :
	require get_template_directory() . '/framework/woocommerce.php';
endif;

/*
** Set up Mega Menu
*/
require get_template_directory() . '/framework/megamenu.php';


/*
** Load Custom Widgets
*/
require get_template_directory() . '/framework/widgets/recent-posts.php';
require get_template_directory() . '/framework/widgets/video.php';
require get_template_directory() . '/framework/widgets/featured-posts.php';
require get_template_directory() . '/framework/widgets/rt-featured-posts.php';
require get_template_directory() . '/framework/widgets/rt-flickr.php';
require get_template_directory() . '/framework/widgets/rt-most-commented.php';
require get_template_directory() . '/framework/widgets/rt-recent-comments.php';
require get_template_directory() . '/framework/widgets/rt-social.php';
require get_template_directory() . '/framework/widgets/rt-twitter.php'; //Got Bugs, Do not Register
//require get_template_directory() . '/framework/widgets/facebook.php';
//require get_template_directory() . '/framework/widgets/gallery.php';

function rt_register_widgets() {
	register_widget('RT_Feature_Posts');
	register_widget('RT_Recent_Comments');
	register_widget('RT_Social_Links');
	//register_widget('RT_Twitter');
	register_widget('RT_Flickr');
	register_widget('RT_Most_Commented');
}
add_action('widgets_init', 'rt_register_widgets', 1);


/**
 * Include Meta Boxes.
 */
require get_template_directory() . '/framework/metaboxes/page-attributes.php';
require get_template_directory() . '/framework/metaboxes/display-options.php';



function my_show_extra_profile_fields( $user ) { ?>

    <h3>Extra profile information</h3>
    <?php
    $post_count =  count_user_posts( $user->ID);
    $rating_res = star_rating($post_count);
    ?>

    <table class="form-table">

        <tr>
            <th><label for="rating">Rating</label></th>

            <td>

                <?php $args = array(
                    'type' => 'rating',
                    'rating' => $rating_res,
                    'number' => 5,

                );?>
                <?php  wp_star_rating($args); ?><br />
                <span class="description">Please Rate you Stars.</span>
            </td>
        </tr>

    </table>
<?php }

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );



add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    /* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
    update_usermeta( $user_id, 'rating', $_POST['rating'] );
}


function star_rating($star){
    $limit_1 = get_theme_mod('protopress_rating_limit_1');
    $limit_2 = get_theme_mod('protopress_rating_limit_2');
    $limit_3 = get_theme_mod('protopress_rating_limit_3');
    $limit_4 = get_theme_mod('protopress_rating_limit_4');
    $limit_5 = get_theme_mod('protopress_rating_limit_5');
    switch($star) {
        case ($star <= $limit_1):
            return 1;
             break;
        case ($star > $limit_1 && $star <= $limit_2):
            return 2;
            break;
        case ($star > $limit_2 && $star <= $limit_3):
            return 3;
            break;
        case ($star > $limit_3 && $star <= $limit_4):
            return 4;
            break;
        case ($star > $limit_4):
            return 5;
            break;
    }
}