<?php
/**
 * Adds a meta box to the post editing screen
 */
function protopress_custom_meta() {
    add_meta_box( 'protopress_meta', __( 'Display Options', 'protopress' ), 'protopress_meta_callback', 'page','side','high' );
}
add_action( 'add_meta_boxes', 'protopress_custom_meta' );

/**
 * Outputs the content of the meta box
 */
 
function protopress_meta_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'protopress_nonce' );
    $protopress_stored_meta = get_post_meta( $post->ID );
    ?>
    
    <p>
	    <div class="protopress-row-content">
	        <label for="enable-slider">
	            <input type="checkbox" name="enable-slider" id="enable-slider" value="yes" <?php if ( isset ( $protopress_stored_meta['enable-slider'] ) ) checked( $protopress_stored_meta['enable-slider'][0], 'yes' ); ?> />
	            <?php _e( 'Enable Slider', 'protopress' )?>
	        </label>
	        <br />
	        <label for="enable-sqbx">
	            <input type="checkbox" name="enable-sqbx" id="enable-sqbx" value="yes" <?php if ( isset ( $protopress_stored_meta['enable-sqbx'] ) ) checked( $protopress_stored_meta['enable-sqbx'][0], 'yes' ); ?> />
	            <?php _e( 'Enable Square Boxes and 3D Slider', 'protopress' )?>
	        </label>
	        <br />
	        <label for="enable-img-grid">
	            <input type="checkbox" name="enable-img-grid" id="enable-img-grid" value="yes" <?php if ( isset ( $protopress_stored_meta['enable-img-grid'] ) ) checked( $protopress_stored_meta['enable-img-grid'][0], 'yes' ); ?> />
	            <?php _e( 'Enable Grid', 'protopress' )?>
	        </label>
	        <br />
	        <label for="hide-title">
	            <input type="checkbox" name="hide-title" id="hide-title" value="yes" <?php if ( isset ( $protopress_stored_meta['hide-title'] ) ) checked( $protopress_stored_meta['hide-title'][0], 'yes' ); ?> />
	            <?php _e( 'Hide Page Title', 'protopress' )?>
	        </label>
	        <br />
	        <label for="enable-full-width">
	            <input type="checkbox" name="enable-full-width" id="enable-full-width" value="yes" <?php if ( isset ( $protopress_stored_meta['enable-full-width'] ) ) checked( $protopress_stored_meta['enable-full-width'][0], 'yes' ); ?> />
	            <?php _e( 'Enable Full Width', 'protopress' )?>
	        </label>
	    </div>
	</p>
 
    <?php
}


/**
 * Saves the custom meta input
 */
function protopress_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'protopress_nonce' ] ) && wp_verify_nonce( $_POST[ 'protopress_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'meta-text' ] ) ) {
        update_post_meta( $post_id, 'meta-text', sanitize_text_field( $_POST[ 'meta-text' ] ) );
    }
    
    // Checks for input and saves
	if( isset( $_POST[ 'enable-slider' ] ) ) {
	    update_post_meta( $post_id, 'enable-slider', 'yes' );
	} else {
	    update_post_meta( $post_id, 'enable-slider', '' );
	}
	
	// Checks for input and saves
	if( isset( $_POST[ 'enable-sqbx' ] ) ) {
	    update_post_meta( $post_id, 'enable-sqbx', 'yes' );
	} else {
	    update_post_meta( $post_id, 'enable-sqbx', '' );
	}
	
	// Checks for input and saves
	if( isset( $_POST[ 'enable-img-grid' ] ) ) {
	    update_post_meta( $post_id, 'enable-img-grid', 'yes' );
	} else {
	    update_post_meta( $post_id, 'enable-img-grid', '' );
	}
	 
	// Checks for input and saves
	if( isset( $_POST[ 'hide-title' ] ) ) {
	    update_post_meta( $post_id, 'hide-title', 'yes' );
	} else {
	    update_post_meta( $post_id, 'hide-title', '' );
	}
	
	// Checks for input and saves
	if( isset( $_POST[ 'enable-full-width' ] ) ) {
	    update_post_meta( $post_id, 'enable-full-width', 'yes' );
	} else {
	    update_post_meta( $post_id, 'enable-full-width', '' );
	}
 
}
add_action( 'save_post', 'protopress_meta_save' );