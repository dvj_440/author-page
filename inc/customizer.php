<?php
/**
 * protopress Theme Customizer
 *
 * @package protopress
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
 
 
function protopress_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	
	
	
	//Basic Theme Settings
	$wp_customize->add_section( 'protopress_basic_settings' , array(
	    'title'      => __( 'Basic Settings', 'protopress' ),
	    'priority'   => 30,
	) );
	
	$wp_customize->add_setting( 'protopress_disable_featimg' , array(
	    'default'     => false,
	    'sanitize_callback' => 'protopress_sanitize_checkbox',
	) );
	
	$wp_customize->add_control(	   
        'protopress_disable_featimg',
        array(
            'label' => 'Disable Featured Images on Posts.',
            'description' => 'This will Remove the Featured Images from Showing up on Individual Posts, however, it will not remove it from homepage and other elements.',
            'section' => 'protopress_basic_settings',
            'settings' => 'protopress_disable_featimg',
            'priority' => 5,
            'type' => 'checkbox',
        )
	);
	
	$wp_customize->add_setting( 'protopress_disable_nextprev' , array(
	    'default'     => true,
	    'sanitize_callback' => 'protopress_sanitize_checkbox',
	) );
	
	
	
	$wp_customize->add_control(	   
        'protopress_disable_nextprev',
        array(
            'label' => 'Disable Next/Prev Posts on Single Posts.',
            'description' => 'This will Remove the the link to next and previous posts on all posts.',
            'section' => 'protopress_basic_settings',
            'settings' => 'protopress_disable_nextprev',
            'priority' => 5,
            'type' => 'checkbox',
        )
	);
	
	
	
	
	
	//Logo Settings
	$wp_customize->add_section( 'title_tagline' , array(
	    'title'      => __( 'Title, Tagline & Logo', 'protopress' ),
	    'priority'   => 30,
	) );
	
	$wp_customize->add_setting( 'protopress_logo' , array(
	    'default'     => '',
	    'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	        $wp_customize,
	        'protopress_logo',
	        array(
	            'label' => 'Upload Logo',
	            'section' => 'title_tagline',
	            'settings' => 'protopress_logo',
	            'priority' => 5,
	        )
		)
	);
	
	$wp_customize->add_setting( 'protopress_logo_resize' , array(
	    'default'     => 100,
	    'sanitize_callback' => 'protopress_sanitize_positive_number',
	) );
	$wp_customize->add_control(
	        'protopress_logo_resize',
	        array(
	            'label' => 'Resize & Adjust Logo',
	            'section' => 'title_tagline',
	            'settings' => 'protopress_logo_resize',
	            'priority' => 6,
	            'type' => 'range',
	            'active_callback' => 'protopress_logo_enabled',
	            'input_attrs' => array(
			        'min'   => 30,
			        'max'   => 200,
			        'step'  => 5,
			    ),
	        )
	);
	
	function protopress_logo_enabled($control) {
		$option = $control->manager->get_setting('protopress_logo');
		return $option->value() == true;
	}
	
	
	
	//Replace Header Text Color with, separate colors for Title and Description
	//Override protopress_site_titlecolor
	$wp_customize->remove_control('display_header_text');
	$wp_customize->remove_control('background_color');
	$wp_customize->remove_section('colors');
	$wp_customize->remove_setting('header_textcolor');
	$wp_customize->add_setting('protopress_site_titlecolor', array(
	    'default'     => '#e10d0d',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_site_titlecolor', array(
			'label' => __('Site Title Color','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_site_titlecolor',
			'type' => 'color'
		) ) 
	);
	
	$wp_customize->add_setting('protopress_header_desccolor', array(
	    'default'     => '#777',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_header_desccolor', array(
			'label' => __('Site Tagline Color','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_header_desccolor',
			'type' => 'color'
		) ) 
	);
	
	//Settings for Nav Area
	$wp_customize->add_section( 'protopress_nav_settings' , array(
	    'panel'     => 'nav_menus',
	    'title' => 'Menu Settings',
	    'priority' => 1
	) );
	
	
	$wp_customize->add_setting( 'protopress_disable_nav_desc' , array(
	    'default'     => false,
	    'sanitize_callback' => 'protopress_sanitize_checkbox',
	) );
	
	
	$wp_customize->add_control(
	'protopress_disable_nav_desc', array(
		'label' => __('Disable Description of Menu Items','protopress'),
		'section' => 'protopress_nav_settings',
		'settings' => 'protopress_disable_nav_desc',
		'type' => 'checkbox'
	) );
	
	$wp_customize->add_setting( 'protopress_disable_active_nav' , array(
	    'default'     => true,
	    'sanitize_callback' => 'protopress_sanitize_checkbox',
	) );
	
	$wp_customize->add_control(
	'protopress_disable_active_nav', array(
		'label' => __('Disable Highlighting of Current Active Item on the Menu.','protopress'),
		'section' => 'protopress_nav_settings',
		'settings' => 'protopress_disable_active_nav',
		'type' => 'checkbox'
	) );
	
	//Settings for Header Image
	$wp_customize->add_setting( 'protopress_himg_style' , array(
	    'default'     => true,
	    'sanitize_callback' => 'protopress_sanitize_himg_style'
	) );
	
	/* Sanitization Function */
	function protopress_sanitize_himg_style( $input ) {
		if (in_array( $input, array('contain','cover') ) )
			return $input;
		else
			return '';	
	}
	
	$wp_customize->add_control(
	'protopress_himg_style', array(
		'label' => __('Header Image Arrangement','protopress'),
		'section' => 'header_image',
		'settings' => 'protopress_himg_style',
		'type' => 'select',
		'choices' => array(
				'contain' => __('Contain','protopress'),
				'cover' => __('Cover Completely','protopress'),
				)
	) );
	
	$wp_customize->add_setting( 'protopress_himg_align' , array(
	    'default'     => true,
	    'sanitize_callback' => 'protopress_sanitize_himg_align'
	) );
	
	/* Sanitization Function */
	function protopress_sanitize_himg_align( $input ) {
		if (in_array( $input, array('center','left','right') ) )
			return $input;
		else
			return '';	
	}
	
	$wp_customize->add_control(
	'protopress_himg_align', array(
		'label' => __('Header Image Alignment','protopress'),
		'section' => 'header_image',
		'settings' => 'protopress_himg_align',
		'type' => 'select',
		'choices' => array(
				'center' => __('Center','protopress'),
				'left' => __('Left','protopress'),
				'right' => __('Right','protopress'),
			)
		
	) );
	
	$wp_customize->add_setting( 'protopress_himg_repeat' , array(
	    'default'     => true,
	    'sanitize_callback' => 'protopress_sanitize_checkbox'
	) );
	
	$wp_customize->add_control(
	'protopress_himg_repeat', array(
		'label' => __('Repeat Header Image','protopress'),
		'section' => 'header_image',
		'settings' => 'protopress_himg_repeat',
		'type' => 'checkbox',
	) );
	
	
	//Settings For Logo Area
	
	$wp_customize->add_setting(
		'protopress_hide_title_tagline',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_hide_title_tagline', array(
		    'settings' => 'protopress_hide_title_tagline',
		    'label'    => __( 'Hide Title and Tagline.', 'protopress' ),
		    'section'  => 'title_tagline',
		    'type'     => 'checkbox',
		)
	);
	
	$wp_customize->add_setting(
		'protopress_branding_below_logo',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_branding_below_logo', array(
		    'settings' => 'protopress_branding_below_logo',
		    'label'    => __( 'Display Site Title and Tagline Below the Logo.', 'protopress' ),
		    'section'  => 'title_tagline',
		    'type'     => 'checkbox',
		    'active_callback' => 'protopress_title_visible'
		)
	);
	
	function protopress_title_visible( $control ) {
		$option = $control->manager->get_setting('protopress_hide_title_tagline');
	    return $option->value() == false ;
	}
	
	$wp_customize->add_setting(
		'protopress_center_logo',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_center_logo', array(
		    'settings' => 'protopress_center_logo',
		    'label'    => __( 'Center Align.', 'protopress' ),
		    'section'  => 'title_tagline',
		    'type'     => 'checkbox',
		)
	);
	
	
	
	// SLIDER PANEL
	$wp_customize->add_panel( 'protopress_slider_panel', array(
	    'priority'       => 35,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '',
	    'title'          => 'Main Slider',
	) );
	
	$wp_customize->add_section(
	    'protopress_sec_slider_options',
	    array(
	        'title'     => 'Enable/Disable',
	        'priority'  => 0,
	        'panel'     => 'protopress_slider_panel'
	    )
	);
	
	
	$wp_customize->add_setting(
		'protopress_main_slider_enable',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_main_slider_enable', array(
		    'settings' => 'protopress_main_slider_enable',
		    'label'    => __( 'Enable Slider on HomePage.', 'protopress' ),
		    'section'  => 'protopress_sec_slider_options',
		    'type'     => 'checkbox',
		)
	);
	
	$wp_customize->add_setting(
		'protopress_main_slider_enable_posts',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_main_slider_enable_posts', array(
		    'settings' => 'protopress_main_slider_enable_posts',
		    'label'    => __( 'Enable Slider on All Posts.', 'protopress' ),
		    'section'  => 'protopress_sec_slider_options',
		    'type'     => 'checkbox',
		)
	);
	
	$wp_customize->add_setting(
		'protopress_main_slider_enable_pages',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_main_slider_enable_pages', array(
		    'settings' => 'protopress_main_slider_enable_pages',
		    'label'    => __( 'Enable Slider on All Pages.', 'protopress' ),
		    'section'  => 'protopress_sec_slider_options',
		    'type'     => 'checkbox',
		)
	);
	
	$wp_customize->add_setting(
		'protopress_main_slider_count',
			array(
				'default' => '0',
				'sanitize_callback' => 'protopress_sanitize_positive_number'
			)
	);
	
	// Select How Many Slides the User wants, and Reload the Page.
	$wp_customize->add_control(
			'protopress_main_slider_count', array(
		    'settings' => 'protopress_main_slider_count',
		    'label'    => __( 'No. of Slides(Min:0, Max: 10)' ,'protopress'),
		    'section'  => 'protopress_sec_slider_options',
		    'type'     => 'number',
		    'description' => __('Save the Settings, and Reload this page to Configure the Slides.','protopress'),
		    
		)
	);
	
	
	//Slider Config
	$wp_customize->add_section(
	    'protopress_slider_config',
	    array(
	        'title'     => __('Configure Slider','protopress'),
	        'priority'  => 0,
	        'panel'     => 'protopress_slider_panel'
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_slider_pause',
			array(
				'default' => 5000,
				'sanitize_callback' => 'protopress_sanitize_positive_number'
			)
	);
	
	$wp_customize->add_control(
			'protopress_slider_pause', array(
		    'settings' => 'protopress_slider_pause',
		    'label'    => __( 'Time Between Each Slide.' ,'protopress'),
		    'section'  => 'protopress_slider_config',
		    'type'     => 'number',
		    'description' => __('Value in Milliseconds. Default: 5000.','protopress'),
		    
		)
	);
	
	$wp_customize->add_setting(
		'protopress_slider_speed',
			array(
				'default' => 500,
				'sanitize_callback' => 'protopress_sanitize_positive_number'
			)
	);
	
	$wp_customize->add_control(
			'protopress_slider_speed', array(
		    'settings' => 'protopress_slider_speed',
		    'label'    => __( 'Animation Speed.' ,'protopress'),
		    'section'  => 'protopress_slider_config',
		    'type'     => 'number',
		    'description' => __('Value in Milliseconds. Default: 500.','protopress'),
		    
		)
	);
	
	$wp_customize->add_setting(
		'protopress_slider_random',
			array(
				'default' => false,
				'sanitize_callback' => 'protopress_sanitize_checkbox'
			)
	);
	
	$wp_customize->add_control(
			'protopress_slider_random', array(
		    'settings' => 'protopress_slider_random',
		    'label'    => __( 'Start Slider from Random Slide.' ,'protopress'),
		    'section'  => 'protopress_slider_config',
		    'type'     => 'checkbox',		    
		)
	);
	
	$wp_customize->add_setting(
		'protopress_slider_pager',
			array(
				'default' => true,
				'sanitize_callback' => 'protopress_sanitize_checkbox'
			)
	);
	
	$wp_customize->add_control(
			'protopress_slider_pager', array(
		    'settings' => 'protopress_slider_pager',
		    'label'    => __( 'Enable Pager.' ,'protopress'),
		    'section'  => 'protopress_slider_config',
		    'type'     => 'checkbox',
		    'description' => __('Pager is the Circles at the bottom, which represent current slide.','protopress'),		    
		)
	);
	
	$wp_customize->add_setting(
		'protopress_slider_autoplay',
			array(
				'default' => true, //Because, in nivo its Force Manual Transitions.
				'sanitize_callback' => 'protopress_sanitize_checkbox'
			)
	);
	
	$wp_customize->add_control(
			'protopress_slider_autoplay', array(
		    'settings' => 'protopress_slider_autoplay',
		    'label'    => __( 'Enable Autoplay.' ,'protopress'),
		    'section'  => 'protopress_slider_config',
		    'type'     => 'checkbox',
		)
	);
	
	$wp_customize->add_setting(
		'protopress_slider_effect',
			array(
				'default' => 'random',
				'sanitize_callback' => 'protopress_sanitize_text'
			)
	);
	
	$earray=array('random','sliceDown','sliceDownLeft','sliceUp','sliceUpLeft','sliceUpDown',
		'sliceUpDownLeft','fold','fade','slideInRight','slideInLeft','boxRandom','boxRain','boxRainReverse','boxRainGrow','boxRainGrowReverse');
		$earray = array_combine($earray, $earray);
	
	$wp_customize->add_control(
			'protopress_slider_effect', array(
		    'settings' => 'protopress_slider_effect',
		    'label'    => __( 'Slider Animation Effect.' ,'protopress'),
		    'section'  => 'protopress_slider_config',
		    'type'     => 'select',
		    'choices' => $earray,
	) );
	
	
	// Select How Many Slides the User wants, and Reload the Page.
	$wp_customize->add_control(
			'protopress_main_slider_count', array(
		    'settings' => 'protopress_main_slider_count',
		    'label'    => __( 'No. of Slides(Min:0, Max: 30)' ,'protopress'),
		    'section'  => 'protopress_sec_slider_options',
		    'type'     => 'number',
		    'description' => __('Save the Settings, and Reload this page to Configure the Slides.','protopress'),
		    
		)
	);
		
	
	if ( get_theme_mod('protopress_main_slider_count') > 0 ) :
		$slides = get_theme_mod('protopress_main_slider_count');
		
		for ( $i = 1 ; $i <= $slides ; $i++ ) :
			
			//Create the settings Once, and Loop through it.
			
			$wp_customize->add_setting(
				'protopress_slide_img'.$i,
				array( 'sanitize_callback' => 'esc_url_raw' )
			);
			
			$wp_customize->add_control(
			    new WP_Customize_Image_Control(
			        $wp_customize,
			        'protopress_slide_img'.$i,
			        array(
			            'label' => '',
			            'section' => 'protopress_slide_sec'.$i,
			            'settings' => 'protopress_slide_img'.$i,			       
			        )
				)
			);
			
			
			$wp_customize->add_section(
			    'protopress_slide_sec'.$i,
			    array(
			        'title'     => 'Slide '.$i,
			        'priority'  => $i,
			        'panel'     => 'protopress_slider_panel'
			    )
			);
			
			$wp_customize->add_setting(
				'protopress_slide_title'.$i,
				array( 'sanitize_callback' => 'sanitize_text_field' )
			);
			
			$wp_customize->add_control(
					'protopress_slide_title'.$i, array(
				    'settings' => 'protopress_slide_title'.$i,
				    'label'    => __( 'Slide Title','protopress' ),
				    'section'  => 'protopress_slide_sec'.$i,
				    'type'     => 'text',
				)
			);
			
			$wp_customize->add_setting(
				'protopress_slide_desc'.$i,
				array( 'sanitize_callback' => 'sanitize_text_field' )
			);
			
			$wp_customize->add_control(
					'protopress_slide_desc'.$i, array(
				    'settings' => 'protopress_slide_desc'.$i,
				    'label'    => __( 'Slide Description','protopress' ),
				    'section'  => 'protopress_slide_sec'.$i,
				    'type'     => 'text',
				)
			);
			
			
			
			$wp_customize->add_setting(
				'protopress_slide_CTA_button'.$i,
				array( 'sanitize_callback' => 'sanitize_text_field' )
			);
			
			$wp_customize->add_control(
					'protopress_slide_CTA_button'.$i, array(
				    'settings' => 'protopress_slide_CTA_button'.$i,
				    'label'    => __( 'Custom Call to Action Button Text(Optional)','protopress' ),
				    'section'  => 'protopress_slide_sec'.$i,
				    'type'     => 'text',
				)
			);
			
			$wp_customize->add_setting(
				'protopress_slide_url'.$i,
				array( 'sanitize_callback' => 'esc_url_raw' )
			);
			
			$wp_customize->add_control(
					'protopress_slide_url'.$i, array(
				    'settings' => 'protopress_slide_url'.$i,
				    'label'    => __( 'Target URL','protopress' ),
				    'section'  => 'protopress_slide_sec'.$i,
				    'type'     => 'url',
				)
			);
			
		endfor;
	
	
	endif;
	
	

	
	// CREATE THE FCA PANEL
	$wp_customize->add_panel( 'protopress_fca_panel', array(
	    'priority'       => 40,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '',
	    'title'          => 'Featured Content Areas',
	    'description'    => '',
	) );
	
	
	//SQUARE BOXES
	$wp_customize->add_section(
	    'protopress_fc_boxes',
	    array(
	        'title'     => 'Square Boxes',
	        'priority'  => 10,
	        'panel'     => 'protopress_fca_panel'
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_box_enable',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_box_enable', array(
		    'settings' => 'protopress_box_enable',
		    'label'    => __( 'Enable Square Boxes & Posts Slider.', 'protopress' ),
		    'section'  => 'protopress_fc_boxes',
		    'type'     => 'checkbox',
		)
	);
	
 
	$wp_customize->add_setting(
		'protopress_box_title',
		array( 'sanitize_callback' => 'sanitize_text_field' )
	);
	
	$wp_customize->add_control(
			'protopress_box_title', array(
		    'settings' => 'protopress_box_title',
		    'label'    => __( 'Title for the Boxes','protopress' ),
		    'section'  => 'protopress_fc_boxes',
		    'type'     => 'text',
		)
	);
 
 	$wp_customize->add_setting(
	    'protopress_box_cat',
	    array( 'sanitize_callback' => 'protopress_sanitize_category' )
	);
	
	$wp_customize->add_control(
	    new WP_Customize_Category_Control(
	        $wp_customize,
	        'protopress_box_cat',
	        array(
	            'label'    => 'Category For Square Boxes.',
	            'settings' => 'protopress_box_cat',
	            'section'  => 'protopress_fc_boxes'
	        )
	    )
	);
	
	$wp_customize->add_setting(
	    'protopress_box_position', array ( 'default' => 'left' )
	);
	
	$wp_customize->add_control(	    
        'protopress_box_position',
        array(
            'label'    => 'Position of Square Boxes.',
            'settings' => 'protopress_box_position',
            'section'  => 'protopress_fc_boxes',
            'type' => 'select',
            'default'=> 'left',
            'choices' => array(
						'right' => __('Right','protopress'),
						'left' => __('Left','protopress'),
						
					)
        )
	);
	
		
	//SLIDER
	$wp_customize->add_section(
	    'protopress_fc_slider',
	    array(
	        'title'     => __('Posts Slider','protopress'),
	        'priority'  => 10,
	        'panel'     => 'protopress_fca_panel',
	        'description' => 'This is the Posts Slider, displayed along with square boxes.',
	    )
	);
	
	
	$wp_customize->add_setting(
		'protopress_slider_title',
		array( 'sanitize_callback' => 'sanitize_text_field' )
	);
	
	$wp_customize->add_control(
			'protopress_slider_title', array(
		    'settings' => 'protopress_slider_title',
		    'label'    => __( 'Title for the Slider', 'protopress' ),
		    'section'  => 'protopress_fc_slider',
		    'type'     => 'text',
		)
	);
	
	
	
/*
	// SETTING TO BE ADDED IN A FUTURE UPDATE.
	
	$wp_customize->add_setting(
		'slider_type'
		array( 'sanitize_callback' => 'protopress_sanitize_slidertype' )
	);
	
	$wp_customize->add_control(
			'slider_type', array(
		    'settings' => 'slider_type',
		    'label'    => __( 'Choose Slider' ),
		    'section'  => 'fc_slider',
		    'type'     => 'select',
		    'choices'  => array(
		    				'3d' => '3D Slider',
		    				'bx' => 'BX Slider'),
		)
	);
*/
	
	$wp_customize->add_setting(
		'protopress_slider_count',
		array( 'sanitize_callback' => 'protopress_sanitize_positive_number' )
	);
	
	$wp_customize->add_control(
			'protopress_slider_count', array(
		    'settings' => 'protopress_slider_count',
		    'label'    => __( 'No. of Posts(Min:3, Max: 10)', 'protopress' ),
		    'section'  => 'protopress_fc_slider',
		    'type'     => 'range',
		    'input_attrs' => array(
		        'min'   => 3,
		        'max'   => 10,
		        'step'  => 1,
		        'class' => 'test-class test',
		        'style' => 'color: #0a0',
		    ),
		)
	);
		
	$wp_customize->add_setting(
		    'protopress_slider_cat',
		    array( 'sanitize_callback' => 'protopress_sanitize_category' )
		);
		
	$wp_customize->add_control(
	    new WP_Customize_Category_Control(
	        $wp_customize,
	        'protopress_slider_cat',
	        array(
	            'label'    => __('Category For Slider.','protopress'),
	            'settings' => 'protopress_slider_cat',
	            'section'  => 'protopress_fc_slider'
	        )
	    )
	);
	
	
	
	//IMAGE GRID
	
	$wp_customize->add_section(
	    'protopress_fc_grid',
	    array(
	        'title'     => __('Image Grid','protopress'),
	        'priority'  => 10,
	        'panel'     => 'protopress_fca_panel'
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_grid_enable',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_grid_enable', array(
		    'settings' => 'protopress_grid_enable',
		    'label'    => __( 'Enable Grid.', 'protopress' ),
		    'section'  => 'protopress_fc_grid',
		    'type'     => 'checkbox',
		)
	);
	
	
	$wp_customize->add_setting(
		'protopress_grid_title',
		array( 'sanitize_callback' => 'sanitize_text_field' )
	);
	
	$wp_customize->add_control(
			'protopress_grid_title', array(
		    'settings' => 'protopress_grid_title',
		    'label'    => __( 'Title for the Grid', 'protopress' ),
		    'section'  => 'protopress_fc_grid',
		    'type'     => 'text',
		)
	);
	
	
	
	$wp_customize->add_setting(
		    'protopress_grid_cat',
		    array( 'sanitize_callback' => 'protopress_sanitize_category' )
		);
	
		
	$wp_customize->add_control(
	    new WP_Customize_Category_Control(
	        $wp_customize,
	        'protopress_grid_cat',
	        array(
	            'label'    => __('Category For Image Grid','protopress'),
	            'settings' => 'protopress_grid_cat',
	            'section'  => 'protopress_fc_grid'
	        )
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_grid_rows',
		array( 'sanitize_callback' => 'protopress_sanitize_positive_number' )
	);
	
	$wp_customize->add_control(
			'protopress_grid_rows', array(
		    'settings' => 'protopress_grid_rows',
		    'label'    => __( 'Max No. of Posts in the Grid. Enter 0 to Disable the Grid.', 'protopress' ),
		    'section'  => 'protopress_fc_grid',
		    'type'     => 'number',
		    'default'  => '0'
		)
	);
	
	
	// Layout and Design
	$wp_customize->add_panel( 'protopress_design_panel', array(
	    'priority'       => 40,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '',
	    'title'          => __('Design & Layout','protopress'),
	) );
	
	$wp_customize->add_section(
	    'protopress_site_layout_sec',
	    array(
	        'title'     => __('Site Layout','protopress'),
	        'priority'  => 0,
	        'panel'     => 'protopress_design_panel'
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_site_layout',
		array( 'sanitize_callback' => 'protopress_sanitize_site_layout' )
	);
	
	function protopress_sanitize_site_layout( $input ) {
		if ( in_array($input, array('full','boxed') ) )
			return $input;
		else 
			return '';	
	}
	
	$wp_customize->add_control(
		'protopress_site_layout',array(
				'label' => __('Select Layout','protopress'),
				'settings' => 'protopress_site_layout',
				'section'  => 'protopress_site_layout_sec',
				'type' => 'select',
				'choices' => array(
						'full' => __('Full Width Layout','protopress'),
						'boxed' => __('Boxed','protopress'),
						
					)
			)
	);
	
	$wp_customize->add_section(
	    'protopress_design_options',
	    array(
	        'title'     => __('Blog Layout','protopress'),
	        'priority'  => 0,
	        'panel'     => 'protopress_design_panel'
	    )
	);
	
	
	$wp_customize->add_setting(
		'protopress_blog_layout',
		array( 'sanitize_callback' => 'protopress_sanitize_blog_layout' )
	);
	
	function protopress_sanitize_blog_layout( $input ) {
		if ( in_array($input, array('grid','grid_2_column','grid_3_column','grid_4_column','photos_1_column','photos_2_column','photos_3_column') ) )
			return $input;
		else 
			return '';	
	}
	
	$wp_customize->add_control(
		'protopress_blog_layout',array(
				'label' => __('Select Layout','protopress'),
				'settings' => 'protopress_blog_layout',
				'section'  => 'protopress_design_options',
				'type' => 'select',
				'choices' => array(
						'grid' => __('Basic Blog Layout','protopress'),
						'grid_2_column' => __('Grid - 2 Column','protopress'),
						'grid_3_column' => __('Grid - 3 Column','protopress'),
						'grid_4_column' => __('Grid - 4 Column','protopress'),
						'photos_1_column' => __('Photography - 1 Column','protopress'),
						'photos_2_column' => __('Photography - 2 Column','protopress'),
						'photos_3_column' => __('Photography - 3 Column','protopress'),
					)
			)
	);
	
	$wp_customize->add_section(
	    'protopress_sidebar_options',
	    array(
	        'title'     => __('Sidebar Layout','protopress'),
	        'priority'  => 0,
	        'panel'     => 'protopress_design_panel'
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_disable_sidebar',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_disable_sidebar', array(
		    'settings' => 'protopress_disable_sidebar',
		    'label'    => __( 'Disable Sidebar Everywhere.','protopress' ),
		    'section'  => 'protopress_sidebar_options',
		    'type'     => 'checkbox',
		    'default'  => false
		)
	);
	
	$wp_customize->add_setting(
		'protopress_disable_sidebar_home',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_disable_sidebar_home', array(
		    'settings' => 'protopress_disable_sidebar_home',
		    'label'    => __( 'Disable Sidebar on Home/Blog.','protopress' ),
		    'section'  => 'protopress_sidebar_options',
		    'type'     => 'checkbox',
		    'active_callback' => 'protopress_show_sidebar_options',
		    'default'  => false
		)
	);
	
	$wp_customize->add_setting(
		'protopress_disable_sidebar_archive',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_disable_sidebar_archive', array(
		    'settings' => 'protopress_disable_sidebar_archive',
		    'label'    => __( 'Disable Sidebar on Archive/Category/Tag Pages.','protopress' ),
		    'section'  => 'protopress_sidebar_options',
		    'type'     => 'checkbox',
		    'active_callback' => 'protopress_show_sidebar_options',
		    'default'  => false
		)
	);
	
	$wp_customize->add_setting(
		'protopress_disable_sidebar_front',
		array( 'sanitize_callback' => 'protopress_sanitize_checkbox' )
	);
	
	$wp_customize->add_control(
			'protopress_disable_sidebar_front', array(
		    'settings' => 'protopress_disable_sidebar_front',
		    'label'    => __( 'Disable Sidebar on Front Page.','protopress' ),
		    'section'  => 'protopress_sidebar_options',
		    'type'     => 'checkbox',
		    'active_callback' => 'protopress_show_sidebar_options',
		    'default'  => false
		)
	);
	
	
	$wp_customize->add_setting(
		'protopress_sidebar_width',
		array(
			'default' => 4,
		    'sanitize_callback' => 'protopress_sanitize_positive_number' )
	);
	
	$wp_customize->add_control(
			'protopress_sidebar_width', array(
		    'settings' => 'protopress_sidebar_width',
		    'label'    => __( 'Sidebar Width','protopress' ),
		    'description' => __('Min: 25%, Default: 33%, Max: 40%','protopress'),
		    'section'  => 'protopress_sidebar_options',
		    'type'     => 'range',
		    'active_callback' => 'protopress_show_sidebar_options',
		    'input_attrs' => array(
		        'min'   => 3,
		        'max'   => 5,
		        'step'  => 1,
		        'class' => 'sidebar-width-range',
		        'style' => 'color: #0a0',
		    ),
		)
	);
	
	$wp_customize->add_setting(
		'protopress_sidebar_loc',
		array(
			'default' => 'right',
		    'sanitize_callback' => 'protopress_sanitize_sidebar_loc' )
	);
	
	$wp_customize->add_control(
			'protopress_sidebar_loc', array(
		    'settings' => 'protopress_sidebar_loc',
		    'label'    => __( 'Sidebar Location','protopress' ),
		    'section'  => 'protopress_sidebar_options',
		    'type'     => 'select',
		    'active_callback' => 'protopress_show_sidebar_options',
		    'choices' => array(
		        'left'   => "Left",
		        'right'   => "Right",
		    ),
		)
	);
	
	/* sanitization */
	function protopress_sanitize_sidebar_loc( $input ) {
		if (in_array($input, array('left','right') ) ) :
			return $input;
		else :
			return '';
		endif;		
	}
	
	/* Active Callback Function */
	function protopress_show_sidebar_options($control) {
	   
	    $option = $control->manager->get_setting('protopress_disable_sidebar');
	    return $option->value() == false ;
	    
	}
	
	class Protopress_Custom_CSS_Control extends WP_Customize_Control {
	    public $type = 'textarea';
	 
	    public function render_content() {
	        ?>
	            <label>
	                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	                <textarea rows="8" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
	            </label>
	        <?php
	    }
	}
	
	class protopress_Custom_JS_Control extends WP_Customize_Control {
	    public $type = 'textarea';
	 
	    public function render_content() {
	        ?>
	            <label>
	                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	                <textarea rows="8" style="width:100%;background: #222; color: #eee;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
	            </label>
	        <?php
	    }
	}
	
	$wp_customize-> add_section(
    'protopress_custom_codes_js',
    array(
    	'title'			=> __('Custom JS','protopress'),
    	'description'	=> __('Enter your Custom JS Code. It will be Included in Head of the Site. Do NOT Include &lt;script&gt; and &lt;/script&gt; tags.','protopress'),
    	'priority'		=> 11,
    	'panel'			=> 'protopress_design_panel'
    	)
    );
    
	$wp_customize->add_setting(
	'protopress_custom_js',
	array(
		'default'		=> '',
		'sanitize_callback'	=> 'protopress_sanitize_text'
		)
	);
	
	$wp_customize->add_control(
	    new protopress_Custom_JS_Control(
	        $wp_customize,
	        'protopress_custom_js',
	        array(
	            'section' => 'protopress_custom_codes_js',
	            'settings' => 'protopress_custom_js'
	        )
	    )
	);
	
	function protopress_sanitize_text( $input ) {
	    return wp_kses_post( force_balance_tags( $input ) );
	}
	
	$wp_customize-> add_section(
    'protopress_custom_footer',
    array(
    	'title'			=> __('Footer Credit Link','protopress'),
    	'description'	=> __('Enter your Own Copyright Text.','protopress'),
    	'priority'		=> 11,
    	'panel'			=> 'protopress_design_panel'
    	)
    );
    
	$wp_customize->add_setting(
	'protopress_footer_text',
	array(
		'default'		=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
		)
	);
	
	$wp_customize->add_control(	 
	       'protopress_footer_text',
	        array(
	            'section' => 'protopress_custom_footer',
	            'settings' => 'protopress_footer_text',
	            'type' => 'text'
	        )
	);
	
	$wp_customize->add_setting(
	'protopress_link_remove',
	array(
		'default'		=> false,
		'sanitize_callback'	=> 'protopress_sanitize_checkbox'
		)
	);
	
	$wp_customize->add_control(	 
	       'protopress_link_remove',
	        array(
	            'section' => 'protopress_custom_footer',
	            'label' => __('Remove Footer Theme Credit Link.','protopress'),
	            'settings' => 'protopress_link_remove',
	            'type' => 'checkbox'
	        )
	);
		
	$wp_customize->add_section(
	    'protopress_woo_options',
	    array(
	        'title'     => __('WooCommerce Layout','protopress'),
	        'priority'  => 0,
	        'panel'     => 'protopress_design_panel'
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_woo_layout', array( 'default' => '3' )
	);
	
	
	$wp_customize->add_control(
		'protopress_woo_layout',array(
				'label' => __('Select Layout','protopress'),
				'settings' => 'protopress_woo_layout',
				'section'  => 'protopress_woo_options',
				'type' => 'select',
				'default' => '3',
				'choices' => array(
						'2' => __('2 Columns','protopress'),
						'3' => __('3 Columns','protopress'),
						'4' => __('4 Columns','protopress'),
					),
			)
	);
	
	$wp_customize->add_setting(
		'protopress_woo_qty', array( 'default' => '12' )
	);
	
	
	$wp_customize->add_control(
		'protopress_woo_qty',array(
				'description' => __('This Value may reflect after you save and re-load the page.','protopress'),
				'label' => __('No of Products per Page','protopress'),
				'settings' => 'protopress_woo_qty',
				'section'  => 'protopress_woo_options',
				'type' => 'number',
				'default' => '12'
				
			)
	);
	
	$wp_customize->add_section(
	    'protopress_typo_options',
	    array(
	        'title'     => __('Google Web Fonts','protopress'),
	        'priority'  => 41,
	        'description' => __('Fonts are Sorted in Order of Popularity. Defaults: Raleway, Khula.','protopress')
	    )
	);
	
	/**
	 * A class to create a dropdown for all google fonts
	 */
	 class Google_Font_Dropdown_Custom_Control extends WP_Customize_Control
	 {
	    private $fonts = false;
	
	    public function __construct($manager, $id, $args = array(), $options = array())
	    {
	        $this->fonts = $this->get_fonts();
	        parent::__construct( $manager, $id, $args );
	    }

	    /**
	     * Render the content of the category dropdown
	     *
	     * @return HTML
	     */
	    public function render_content()
	    {
	        if(!empty($this->fonts))
	        {
	            ?>
	                <label>
	                    <span class="customize-category-select-control" style="font-weight: bold; display: block; padding: 5px 0px;"><?php echo esc_html( $this->label ); ?><br /></span>
	                    
	                    <select <?php $this->link(); ?>>
	                        <?php
	                            foreach ( $this->fonts as $k => $v )
	                            {
	                               printf('<option value="%s" %s>%s</option>', $v->family, selected($this->value(), $k, false), $v->family);
	                            }
	                        ?>
	                    </select>
	                </label>
	            <?php
	        }
	    }
	
	    /**
	     * Get the google fonts from the API or in the cache
	     *
	     * @param  integer $amount
	     *
	     * @return String
	     */
	    public function get_fonts( $amount = 'all' )
	    {
	        $fontFile = get_template_directory().'/inc/cache/google-web-fonts.txt';
	
	        //Total time the file will be cached in seconds, set to a week
	        $cachetime = 86400 * 30;
	
	        if(file_exists($fontFile) && $cachetime < filemtime($fontFile))
	        {
	            $content = json_decode(file_get_contents($fontFile));
	           
	        } else {
	
	            $googleApi = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key=AIzaSyCnUNuE7iJyG-tuhk24EmaLZSC6yn3IjhQ';
	
	            $fontContent = wp_remote_get( $googleApi, array('sslverify'   => false) );
	
	            $fp = fopen($fontFile, 'w');
	            fwrite($fp, $fontContent['body']);
	            fclose($fp);
	
	            $content = json_decode($fontContent['body']);
	            
	        }
	
	        if($amount == 'all')
	        {
	            return $content->items;
	        } else {
	            return array_slice($content->items, 0, $amount);
	        }
	        
	    }
	 }
	
	
	
	$wp_customize->add_setting(
		'protopress_title_font' ,array('default' => 'Raleway')
	);
	
	$wp_customize->add_control( new Google_Font_Dropdown_Custom_Control(
		$wp_customize,
		'protopress_title_font',array(
				'label' => __('Title Font','protopress'),
				'settings' => 'protopress_title_font',
				'section'  => 'protopress_typo_options',
			)
		)
	);
	
	
	$wp_customize->add_setting(
		'protopress_body_font'
	);
	
	$wp_customize->add_control(
		new Google_Font_Dropdown_Custom_Control(
		$wp_customize,
		'protopress_body_font',array(
				'label' => __('Body Font','protopress'),
				'settings' => 'protopress_body_font',
				'section'  => 'protopress_typo_options'
			)
		)	
	);
	
//Select the Default Theme Skin
	$wp_customize->add_section(
	    'protopress_skinx_options',
	    array(
	        'title'     => __('Skin Designer','protopress'),
	        'priority'  => 39,
	    )
	);
	
	$wp_customize->add_setting(
		'protopress_skinx',
		array(
			'default'=> 'default',
			
			'sanitize_callback' => 'protopress_sanitize_skin' 
			)
	);
	
	$skins = array( 'default' => __('Default(blue)','protopress'),
					'orange' =>  __('Orange','protopress'),
					'brown' =>  __('Brown','protopress'),
					'green' => __('Green','protopress'),
					'grayscale' => __('GrayScale','protopress'),
					'custom' => __('BUILD CUSTOM SKIN','protopress') );
	
	$wp_customize->add_control(
		'protopress_skinx',array(
				'settings' => 'protopress_skinx',
				'label' => __('Chose a Skin, or Build your Own.','protopress'),
				'section'  => 'protopress_skinx_options',
				'type' => 'select',
				'choices' => $skins,
			)
	);
	
	function protopress_sanitize_skin( $input ) {
		if ( in_array($input, array('default','orange','brown','green','grayscale','custom') ) )
			return $input;
		else
			return '';
	}
	
	//CUSTOM SKIN BUILDER
	
	
	
	$wp_customize->add_setting('protopress_skinx_var_background', array(
	    'default'     => '#FFF',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_skinx_var_background', array(
			'label' => __('Primary Background','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_skinx_var_background',
			'active_callback' => 'protopress_skinx_custom',
			'type' => 'color'
		) ) 
	);
	
	$wp_customize->add_setting('protopress_skinx_var_sbg', array(
	    'default'     => '#F4F4F4',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_skinx_var_sbg', array(
			'label' => __('Secondary Background.','protopress'),
			'description' => __('Should be similar to Primary Background.(Used on Menu, Top bar, etc)','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_skinx_var_sbg',
			'type' => 'color',
			'active_callback' => 'protopress_skinx_custom',
		) ) 
	);
	
	$wp_customize->add_setting('protopress_skinx_var_accent', array(
	    'default'     => '#42a1cd',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_skinx_var_accent', array(
			'label' => __('Primary Accent','protopress'),
			'description' => __('For Most Users, Changing this only color is sufficient.','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_skinx_var_accent',
			'type' => 'color',
			'active_callback' => 'protopress_skinx_custom',
		) ) 
	);
	
	$wp_customize->add_setting('protopress_skinx_var_onaccent', array(
	    'default'     => '#FFFFFF',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_skinx_var_onaccent', array(
			'label' => __('Contrast','protopress'),
			'description' => __('If Accent is Light, this should be dark & vice-versa. This is the Text Color, for places where background color is primary accent.','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_skinx_var_onaccent',
			'type' => 'color',
			'active_callback' => 'protopress_skinx_custom',
		) ) 
	);
	
	
	$wp_customize->add_setting('protopress_skinx_var_headerbg', array(
	    'default'     => '#ffffff',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_skinx_var_headerbg', array(
			'label' => __('Header Background','protopress'),
			'description' => __('This is Similar to Primary Accent By Default.','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_skinx_var_headerbg',
			'type' => 'color',
			'active_callback' => 'protopress_skinx_custom',
		) ) 
	);
	
	
	
	
	
	$wp_customize->add_setting('protopress_skinx_var_content', array(
	    'default'     => '#777777',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'protopress_skinx_var_content', array(
			'label' => __('Content Color','protopress'),
			'description' => __('Must be Dark, like Black or Dark grey. Any darker color is acceptable.','protopress'),
			'section' => 'protopress_skinx_options',
			'settings' => 'protopress_skinx_var_content',
			'active_callback' => 'protopress_skinx_custom',
			'type' => 'color'
		) ) 
	);
	
	function protopress_skinx_custom( $control ) {
		$option = $control->manager->get_setting('protopress_skinx');
	    return $option->value() == 'custom' ;
	}	
	// Social Icons
	$wp_customize->add_section('protopress_social_section', array(
			'title' => __('Social Icons','protopress'),
			'priority' => 44 ,
	));
	
	$social_networks = array( //Redefinied in Sanitization Function.
					'none' => __('-','protpress'),
					'facebook' => __('Facebook','protopress'),
					'twitter' => __('Twitter','protopress'),
					'google-plus' => __('Google Plus','protopress'),
					'instagram' => __('Instagram','protopress'),
					'rss' => __('RSS Feeds','protopress'),
					'vine' => __('Vine','protopress'),
					'vimeo-square' => __('Vimeo','protopress'),
					'youtube' => __('Youtube','protopress'),
					'flickr' => __('Flickr','protopress'),
					'android' => __('Android','protopress'),
					'apple' => __('Apple','protopress'),
					'dribbble' => __('Dribbble','protopress'),
					'foursquare' => __('FourSquare','protopress'),
					'git' => __('Git','protopress'),
					'linkedin' => __('Linked In','protopress'),
					'paypal' => __('PayPal','protopress'),
					'pinterest-p' => __('Pinterest','protopress'),
					'reddit' => __('Reddit','protopress'),
					'skype' => __('Skype','protopress'),
					'soundcloud' => __('SoundCloud','protopress'),
					'tumblr' => __('Tumblr','protopress'),
					'windows' => __('Windows','protopress'),
					'wordpress' => __('WordPress','protopress'),
					'yelp' => __('Yelp','protopress'),
					'vk' => __('VK.com','protopress'),
				);
				
	$social_count = count($social_networks);
				
	for ($x = 1 ; $x <= 10 ; $x++) :
			
		$wp_customize->add_setting(
			'protopress_social_'.$x, array(
				'sanitize_callback' => 'protopress_sanitize_social',
				'default' => 'none'
			));

		$wp_customize->add_control( 'protopress_social_'.$x, array(
					'settings' => 'protopress_social_'.$x,
					'label' => __('Icon ','protopress').$x,
					'section' => 'protopress_social_section',
					'type' => 'select',
					'choices' => $social_networks,			
		));
		
		$wp_customize->add_setting(
			'protopress_social_url'.$x, array(
				'sanitize_callback' => 'esc_url_raw'
			));

		$wp_customize->add_control( 'protopress_social_url'.$x, array(
					'settings' => 'protopress_social_url'.$x,
					'description' => __('Icon ','protopress').$x.__(' Url','protopress'),
					'section' => 'protopress_social_section',
					'type' => 'url',
					'choices' => $social_networks,			
		));
		
	endfor;
	
	function protopress_sanitize_social( $input ) {
		$social_networks = array(
					'none' ,
					'facebook',
					'twitter',
					'google-plus',
					'instagram',
					'rss',
					'vine',
					'vimeo-square',
					'youtube',
					'flickr',
					'android',
					'apple',
					'dribbble',
					'foursquare',
					'git',
					'linkedin',
					'paypal',
					'pinterest-p',
					'reddit',
					'skype',
					'soundcloud',
					'tumblr',
					'windows',
					'wordpress',
					'yelp',
					'vk'
				);
		if ( in_array($input, $social_networks) )
			return $input;
		else
			return '';	
	}
	
	// Advertisement
	
	class Protopress_Custom_Ads_Control extends WP_Customize_Control {
	    public $type = 'textarea';
	 
	    public function render_content() {
	        ?>
	            <label>
	                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	                <span class="customize-control-description"><?php echo esc_html( $this->description ); ?></span>
	                <textarea rows="10" style="width:100%;" <?php $this->link(); ?>><?php echo $this->value(); ?></textarea>
	            </label>
	        <?php
	    }
	}
	
	$wp_customize->add_section('protopress_ads', array(
			'title' => __('Advertisement','protopress'),
			'priority' => 44 ,
	));
	
	$wp_customize->add_setting(
	'protopress_topad',
	array(
		'default'		=> '',
		'sanitize_callback'	=> 'protopress_sanitize_ads'
		)
	);
	
	$wp_customize->add_control(
	    new Protopress_Custom_Ads_Control(
	        $wp_customize,
	        'protopress_topad',
	        array(
	            'section' => 'protopress_ads',
	            'settings' => 'protopress_topad',
	            'label'   => __('Top Ad','protopress'),
	            'description' => __('Enter your Responsive Adsense Code. For Other Ads use 468x60px Banner.','protopress')
	        )
	    )
	);
	
	function protopress_sanitize_ads( $input ) {
		  global $allowedposttags;
	      $custom_allowedtags["script"] = array();
	      $custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
	      $output = wp_kses( $input, $custom_allowedtags);
	      return $output;
	}
	
	//Analytics
	$wp_customize-> add_section(
    'protopress_analytics_js',
    array(
    	'title'			=> __('Google Analytics','protopress'),
    	'description'	=> __('Enter your Analytics Code. It will be Included in Footer of the Site. Do NOT Include &lt;script&gt; and &lt;/script&gt; tags.','protopress'),
    	'priority'		=> 45,
    	)
    );
    
	$wp_customize->add_setting(
	'protopress_analytics',
	array(
		'default'		=> '',
		'sanitize_callback'	=> 'protopress_sanitize_text'
		)
	);
	
	$wp_customize->add_control(
	    new Protopress_Custom_JS_Control(
	        $wp_customize,
	        'protopress_analytics',
	        array(
	            'section' => 'protopress_analytics_js',
	            'settings' => 'protopress_analytics'
	        )
	    )
	);
    //Contributor Rating start
//Basic Theme Settings
    $wp_customize->add_section( 'protopress_rating_section' , array(
        'title'      => __( 'Contributor Rating', 'protopress' ),
        'priority'   => 50,
    ) );
    //limit 1
        $wp_customize->add_setting('protopress_rating_limit_1', array(
            'default' => 0,
            'sanitize_callback' => 'absint',
        ));

        $wp_customize->add_control('protopress_rating_limit_1',
            array(
                'label' => __('Limit: Rating Star 1', 'protopress'),
                'section' => 'protopress_rating_section',
                'settings' => 'protopress_rating_limit_1',
                'type' => 'number'
            )
        );
    //limit 2
    $wp_customize->add_setting('protopress_rating_limit_2', array(
        'default' => 0,
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control('protopress_rating_limit_2',
        array(
            'label' => __('Limit: Rating Star 2', 'protopress'),
            'section' => 'protopress_rating_section',
            'settings' => 'protopress_rating_limit_2',
            'type' => 'number'
        )
    );
    //limit 3
    $wp_customize->add_setting('protopress_rating_limit_3', array(
        'default' => 0,
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control('protopress_rating_limit_3',
        array(
            'label' => __('Limit: Rating Star 3', 'protopress'),
            'section' => 'protopress_rating_section',
            'settings' => 'protopress_rating_limit_3',
            'type' => 'number'
        )
    );

    //limit 4
    $wp_customize->add_setting('protopress_rating_limit_4', array(
        'default' => 0,
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control('protopress_rating_limit_4',
        array(
            'label' => __('Limit: Rating Star 4', 'protopress'),
            'section' => 'protopress_rating_section',
            'settings' => 'protopress_rating_limit_4',
            'type' => 'number'
        )
    );
    //limit 4
    $wp_customize->add_setting('protopress_rating_limit_5', array(
        'default' => 0,
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control('protopress_rating_limit_5',
        array(
            'label' => __('Limit: Rating Star 5', 'protopress'),
            'section' => 'protopress_rating_section',
            'settings' => 'protopress_rating_limit_5',
            'type' => 'number'
        )
    );



    //Contributor Rating End
	
	/* Sanitization Functions Common to Multiple Settings go Here, Specific Sanitization Functions are defined along with add_setting() */
	function protopress_sanitize_checkbox( $input ) {
	    if ( $input == 1 ) {
	        return 1;
	    } else {
	        return '';
	    }
	}
	
	function protopress_sanitize_positive_number( $input ) {
		if ( ($input >= 0) && is_numeric($input) )
			return $input;
		else
			return '';	
	}
	
	function protopress_sanitize_category( $input ) {
		if ( term_exists(get_cat_name( $input ), 'category') )
			return $input;
		else 
			return '';	
	}


}
add_action( 'customize_register', 'protopress_customize_register' );


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function protopress_customize_preview_js() {
	wp_enqueue_script( 'protopress_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'protopress_customize_preview_js' );
