<?php get_header(); ?>

    <div id="primary" class="content-areas <?php do_action('protopress_primary-width') ?>">
        <main id="main" class="site-main" role="main">
            <div id="content" class="narrowcolumn">
                <?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));?>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-12 author-profile">
                        <?php echo get_avatar($curauth->ID,$size = 200); ?>
                        <?php $post_count = count_user_posts( $curauth->ID );
                        $rating_res = star_rating($post_count);
                        //print star start?>
                        <div class="rating-author">
                            <div class='star-blank'>
                            <?php for($i = 1; $i <= 5; $i++){
                                echo "<i class='far fa-star'></i>";
                            }?>
                            </div>
                            <div class='star-fill'>
                            <?php
                            for($i = 1; $i <= $rating_res; $i++){
                                echo "<i class='fas fa-star'></i>";
                            }?>
                            </div>
                        </div>

                        <div class="details"><h4>Name : </h4><div class="del-val"><span><?php echo $curauth->first_name  . $curauth-> last_name; ?></span></div></div>
                        <div class="details"><h4>Email : </h4><div class="del-val"><span><?php echo $curauth->user_email; ?></span></div></div>
                        <div class="details"><h4>WebSite : </h4><div class="del-val"><span><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></span></div></div>
                        <div class="details"><h4>Post Count : </h4><div class="del-val"><span><?php echo count_user_posts( $curauth->ID ).' Post'; ?></span></div></div>
                        <div class="details"><h4><?php _e( 'Posted in' ); ?></h4><div class="del-val"><span><?php the_category( ', ' ); ?></span></div></div>
                    </div>
                    <div class="col-md-8 col-sm-12 author-posts">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <?php get_template_part('framework/layouts/content', 'grid');?>
                        <?php endwhile;
                           // protopress_pagination_queried( $curauth );
                        else : ?>
                            <div class="no-posts">
                                <h2><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></h2>
                                <img src="<?php echo get_template_directory_uri()."/assets/images/sad.png"; ?>">
                            </div>
                        <?php endif; ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php get_footer(); ?>