<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package protopress
 */

get_header(); ?>

	<div id="primary" class="content-area col-md-12 page-404">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page was not be found. Maybe, you should try searching on this site.', 'protopress' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
				
					<?php get_search_form(); ?>
				
					<p class="try"><?php _e( 'Or try one of the links below.', 'protopress' ); ?></p>

					
					<?php the_widget( 'protopress_Recent_Posts', 'no_of_posts=9' ); ?>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
