<?php
/**
 * The template for displaying all page with a Blog Style.
 * Template Name: Blog (Standard Layout)
 *
 * @package protopress
 */

get_header(); ?>

	<div id="primary" class="content-areas <?php do_action('protopress_primary-width') ?>">
		<main id="main" class="site-main" role="main">
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
